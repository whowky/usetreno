<?php

namespace App\Controller;

use App\Form\LoanCalculatorType;
use App\Modules\ApiHashBuilder;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class LoanController extends AbstractController
{
    /**
     * @Route("/", name="loan")
     */
    public function index()
    {
        $loanForm = $this->createForm(LoanCalculatorType::class, null, [
            'action' => $this->generateUrl('calculate'),
            'method' => 'POST'
        ]);

        return $this->render('loan/index.html.twig', [
            'controller_name' => 'výpočet půjčky',
            'loan_form' => $loanForm->createView()
        ]);
    }

    /**
     * @Route("/calculate", name="calculate")
     */
    public function calculateLoan(Request $request)
    {
        $loanData = $request->request->get('loan_calculator');

        try {
            $rcHash = (new ApiHashBuilder($loanData['personalId']))->getHash();
        } catch (HttpException $e) {
            return $this->render('error/serverError.html.twig', [
                'error' => $e->getMessage()
            ]);
        }


        return $this->render('loan/result.html.twig', [
            'controller_name' => 'výsledek',
            'test' => $rcHash
        ]);
    }
}
