<?php
/**
 * Created by PhpStorm.
 * User: jarda
 * Date: 10.11.18
 * Time: 0:10
 */

namespace App\Modules;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiHashBuilder
{
    const ENDPOINT_URL = 'http://www.toppojisteni.net/zadani/rest/rc.php?rc=';

    private $hash = null;

    private $rc = null;

    /**
     * ApiHashBuilder constructor.
     * @param string $rc
     */
    public function __construct(string $rc)
    {
        $this->rc = $rc;
        $this->queryHash();
    }

    /**
     *
     */
    private function queryHash()
    {
        $client = new Client();

        try {
            $res = $client->request('GET', self::ENDPOINT_URL . $this->rc);
        } catch (GuzzleException $e) {
            throw new HttpException(500, 'RC endpoint is not available');
        }

        if ($res->getStatusCode() !== 200) {
            throw new HttpException(500, 'RC endpoint is not available');
        }

        $hash = json_decode($res->getBody()->getContents(), true);
        if ($hash['hash'] !== null) {
            $this->hash = $hash['hash'];
        } else {
            throw new HttpException(500, 'RC not found');
        }
    }

    /**
     * @return null
     */
    public function getHash()
    {
        return $this->hash;
    }


}