<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LoanInputRepository")
 */
class LoanInput
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $propertyPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $loanAmount;

    /**
     * @ORM\Column(type="smallint")
     */
    private $repayment;

    /**
     * @ORM\Column(type="smallint")
     */
    private $fixationTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $personalId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPropertyPrice(): ?int
    {
        return $this->propertyPrice;
    }

    public function setPropertyPrice(int $propertyPrice): self
    {
        $this->propertyPrice = $propertyPrice;

        return $this;
    }

    public function getLoanAmount(): ?int
    {
        return $this->loanAmount;
    }

    public function setLoanAmount(int $loanAmount): self
    {
        $this->loanAmount = $loanAmount;

        return $this;
    }

    public function getRepayment(): ?int
    {
        return $this->repayment;
    }

    public function setRepayment(int $repayment): self
    {
        $this->repayment = $repayment;

        return $this;
    }

    public function getFixationTime(): ?int
    {
        return $this->fixationTime;
    }

    public function setFixationTime(int $fixationTime): self
    {
        $this->fixationTime = $fixationTime;

        return $this;
    }

    public function getPersonalId(): ?string
    {
        return $this->personalId;
    }

    public function setPersonalId(string $personalId): self
    {
        $this->personalId = $personalId;

        return $this;
    }
}
