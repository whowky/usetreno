<?php

namespace App\Repository;

use App\Entity\LoanInput;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LoanInput|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoanInput|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoanInput[]    findAll()
 * @method LoanInput[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanInputRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LoanInput::class);
    }

    // /**
    //  * @return LoanInput[] Returns an array of LoanInput objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LoanInput
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
